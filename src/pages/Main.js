import React, { Component, Fragment } from 'react'
import { Row, Col } from 'antd'

import SearchBar from '../layouts/SearchBar'

export default class Stats extends Component {

    render() {
        return (
            <Fragment>
                <Row type="flex" justify="center">
                    <Col className="text-center" xs={20} sm={18} lg={14} xl={12}>
                        <h1 className="logo-text">PUBG Stats</h1>
                        <hr className="brace"/>
                    </Col>
                </Row>
                <Row type="flex" justify="center">
                    <Col xs={20} sm={18} lg={14} xl={12}>
                        <SearchBar/>
                    </Col>
                </Row>
            </Fragment>
        )
    }
}
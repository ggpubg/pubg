import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { Row, Col, Progress, Spin } from 'antd'

class Stats extends Component {
    state = {
        username: this.props.match.params.username,
        stats: null
    }

    constructor(props) {
        super(props)
        this.props.dispatch({type: "fetchStats", username: props.match.params.username})
    }

    componentDidMount() {
    }

    componentDidUpdate() {
    }

    shouldComponentUpdate(nextProps, nextState) {
        const prev = this.props.match.params.username
        const next = nextProps.match.params.username
        if(prev !== next){
            this.props.dispatch({type: "fetchStats", username: next})
            return true
        }
        return false
    }

    render() {
        const { username } = this.props.match.params
        const { stats } = this.state
        /* */
        return (
            <Fragment>
                <Row>
                    <Col className="text-center" xs={24} sm={24} md={{span:14, offset:5}} lg={{span:14, offset:5}}>
                        <h1 className="logo-text">{username}</h1>
                        { stats === {} ? null : 
                            <Progress percent={100} style={{ marginLeft: '8px' }} status="active" format={() => <Spin />}/>
                        }
                    </Col>
                    <Col className="text-center" xs={24} sm={24} md={{span:14, offset:5}} lg={{span:14, offset:5}}>
                    </Col>
                </Row>
            </Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        host: state.host
    }
}

export default connect(mapStateToProps)(Stats)
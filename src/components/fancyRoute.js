import React, { Component } from 'react'
import { Route } from 'react-router-dom'
import nprogress from 'nprogress'
import 'nprogress/nprogress.css'
import './nprogress.css'

class FancyRoute extends Component {
  constructor(props) {
    super(props)
    nprogress.start()
  }

  componentWillMount () {
  }

  componentDidMount () {
    nprogress.done()
  }

  render () {
    return (
      <Route {...this.props} />
    )
  }
}

export default FancyRoute
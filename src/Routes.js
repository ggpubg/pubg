import asyncComponent from './components/asyncComponent'

const Temp = asyncComponent(() => import("./pages/Temp"))
const Main = asyncComponent(() => import("./pages/Main"))
const Stats = asyncComponent(() => import("./pages/Stats"))
const NoMatch = asyncComponent(() => import("./pages/NoMatch"))

export default [
    {
        title: 'Main',
        path: '/',
        exact: true,
        component:Main
    },
    {
        title: 'Rank',
        path: '/rank',
        exact: true,
        component: Temp
    },
    {
        title: 'Stats',
        path: '/stats',
        exact: true,
        component: Temp
    },
    {
        title: 'Stats',
        path: '/stats/:username',
        exact: false,
        component: Stats
    },
    {
        title: 'Favorite',
        path: '/favorite',
        exact: true,
        component: Temp
    },
    {
        title: 'Stream',
        path: '/stream',
        exact: true,
        component: Temp
    },
    {
        title: 'Stream',
        path: '/stream/:id',
        exact: false,
        component: Temp
    },
    {
        title: 'Notice',
        path: '/notice',
        exact: true,
        component: Temp
    },
    {
        title: 'Notice',
        path: '/notice/:id',
        exact: false,
        component: Temp
    },
    {
        title: 'Support',
        path: '/support',
        exact: true,
        component: Temp
    },
    {
        title: 'NotFound',
        component: NoMatch,
        status: 404
    },
]
/*
<Route exact path="/" component={Main}/>
<Route exact path="/rank" component={Temp}/>
<Route exact path="/stats" component={Temp}/>
<Route exact path="/favorite" component={Temp}/>
<Route exact path="/stream" component={Temp}/>
<Route path="/stream/:id" component={Temp}/>
<Route path="/stats/:username" component={Stats}/>
<Route exact path="/notice" component={Temp}/>
<Route path="/notice/:id" component={Temp}/>
<Route exact path="/support" component={Temp}/>
<Route component={NoMatch} status={404}/>
*/
import React, { Component, Fragment } from 'react'
import { Layout, Icon, Popover, Row, Button, Col } from 'antd'
import PropTypes from 'prop-types'
import { withRouter, Link } from 'react-router-dom'

import Menu from './Menu'
import SearchBar from './SearchBar'

const { Header } = Layout

/*
{menuMode === 'inline' ? (
    <Fragment>
        <Row type="flex" justify="center" className="logo">
        <Link to="/" onClick={this.onClickLogo}>
            <span className="logo-text">PUBGstats</span>
        </Link>
        </Row>
        <Divider dashed/>
    </Fragment>
) : null}
*/

class H extends Component {
    static propTypes = {
        match: PropTypes.object.isRequired,
        location: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired
    }

    state = {
        path: this.props.location.pathname,//.split('/')[1] || '/'
        collapsed: false,
        menuVisible: false
    }

    onClickLogo() {
        this.props.history.push('/')
    }
    
    handleShowMenu = () => {
        this.setState({
          menuVisible: true,
        })
    }

    onMenuVisibleChange = (visible) => {
        this.setState({
          menuVisible: visible,
        })
    }

    render() {
        const { menuMode } = this.props
        const { menuVisible, path } = this.state
        const menu = <Menu menuMode={menuMode} path={path} />
        return (
            <Fragment>
            {
                /* 'inline' === desktop
                * 'horizontal' === mobile
                */
            }
            <Header id="header" className="navbar">
                {menuMode === 'horizontal' ? (
                    <Fragment>
                        <Row type="flex" justify="center" className="ant-menu-dark" style={{ padding: '10px' }}>
                            <Popover
                                overlayClassName="popover-menu"
                                placement="bottom"
                                content={menu}
                                trigger="click"
                                visible={menuVisible}
                                arrowPointAtCenter
                                onVisibleChange={this.onMenuVisibleChange}
                            >
                                <Button ghost onClick={this.handleShowMenu}>
                                <Icon
                                    className="nav-phone-icon"
                                    type="menu-fold"
                                />
                                <span>PUBG</span>
                                </Button>
                            </Popover>
                        </Row>
                    </Fragment>
                ) : null }
              
                  <Row type="flex" justify="start" className="ant-menu-dark">
                        <Col xs={0} md={4}>
                            
                                    <Fragment>
                                    <Link to="/" onClick={this.onClickLogo}>
                                        <span className="logo-text">PUBGstats</span>
                                    </Link>
                                    </Fragment>
                            
                        </Col>
                        <Col className="text-center" xs={24} md={10}>
                            <SearchBar />
                        </Col>
                  </Row>
              </Header>
          </Fragment>
      )
    }
  }

export default withRouter(H)
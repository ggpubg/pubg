import React, { Component } from 'react'
import { Input } from 'antd'
import { withRouter } from 'react-router-dom'

class SearchBar extends Component {

    onSearchStats = (value) => {
        if(!value) return null
        const path = `/stats/${value}`
        this.props.history.push(path)
    }

    render() {
        return (
            
                <Input.Search
                    placeholder="배틀그라운드 닉네임"
                    onSearch={this.onSearchStats}
                    enterButton
                />
        )
    }
}

export default withRouter(SearchBar)
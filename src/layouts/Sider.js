import React, { Component, Fragment } from 'react'
import { Layout } from 'antd'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'
import Menu from './Menu'

const { Sider } = Layout

class S extends Component {
    static propTypes = {
        match: PropTypes.object.isRequired,
        location: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired
    }

    state = {
        path: this.props.location.pathname,//.split('/')[1] || '/'
        collapsed: false,
        menuMode: 'inline'
    }

    onCollapse = (collapsed) => {
        this.setState({ collapsed })
    }

    onClickLogo = () => {
        this.setState({
            path: "/"
        })
    }
    
    linkTo = (item) => {
        this.setState({
            path: item.key
        })
    }

    render() {
      const { menuMode } = this.props
      const { path } = this.state
      return (
          <Fragment>
          {
              /* 'inline' === desktop
              * 'horizontal' === mobile
              */
          }
          {
              menuMode === 'inline' ? (
                  <Sider
                    collapsible
                    collapsed={this.state.collapsed}
                    onCollapse={this.onCollapse}
                    breakpoint="lg"
                  >
                  <Menu menuMode={menuMode} path={path}/>
              </Sider>
          ) : null }
          </Fragment>
      )
    }
  }

export default withRouter(S)
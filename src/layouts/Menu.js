import React, { Component, Fragment } from 'react'
import { Menu, Icon, Divider } from 'antd'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'

class M extends Component {
    static propTypes = {
        match: PropTypes.object.isRequired,
        location: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired
    }

    state = {
        path: this.props.location.pathname,//.split('/')[1] || '/'
    }

    onClickLogo = () => {
        this.setState({
          path: "/"
        })
    }

    linkTo = (item) => {
        this.setState({
          path: item.key
        })
    }

    render() {
        const { menuMode } = this.props
        const { path } = this.state
        return (
            <Fragment>
            {
                /* disable logo on mobile mode */
            }
            <Divider style={{margin: '2px 0'}}/>
            <Menu theme="dark" defaultSelectedKeys={[path]} selectedKeys={[path]} mode={menuMode} onClick={this.linkTo}>
                <Menu.Item key="/">
                <Link to="/">
                    <Icon type="search" />
                    <span>검색</span>
                </Link>
                </Menu.Item>
                <Menu.Item key="/favorite">
                <Link to="/favorite">
                    <Icon type="star" />
                    <span>즐겨찾기</span>
                </Link>
                </Menu.Item>
                <Menu.Item key="/rank">
                <Link to="/rank">
                    <Icon type="profile" />
                    <span>리더보드</span>
                </Link>
                </Menu.Item>
                <Menu.Item key="/stream">
                <Link to="/stream">
                    <Icon type="desktop" />
                    <span>방송</span>
                </Link>
                </Menu.Item>
                <Menu.Item key="/notice">
                <Link to="/notice">
                    <Icon type="check" />
                    <span>공지사항</span>
                </Link>
                </Menu.Item>
                <Menu.Item key="/support">
                <Link to="/support">
                    <Icon type="user" />
                    <span>지원</span>
                </Link>
                </Menu.Item>
            </Menu>
            </Fragment>
        )
    }
}

export default withRouter(M)
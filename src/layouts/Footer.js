import React, { Component } from 'react'
import { Row, Col, Layout, Icon } from 'antd'

class Footer extends Component {

    render() {
        return (
            <Layout.Footer style={{ background: 'none' }}>
                <Row>
                    <Col className="logo-text text-center">
                        <Icon type="copyright" />
                        <span> SEXSEXSEX. All Rights Reserved.</span>
                    </Col>
                </Row>
            </Layout.Footer>
        )
    }
}

export default Footer
/* action types, constants */

const hostname = window && window.location && window.location.hostname

const host = {
    localhost : 'http://localhost:4000',
    prod : 'https://api.pubg.leichtjoon.site'
}[hostname || 'localhost']

export {
    host
}
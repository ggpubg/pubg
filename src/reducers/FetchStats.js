import Axios from 'axios'
import immutable from 'immutable'

import { host } from './Constant'

async function fetch(username) {
    return await Axios({
        method: "GET",
        url: host + '/stats/' + username,
        headers: {
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE"
        }
    })
}

export default async function FetchStats(state = { host }, action) {
    switch(action.type) {
        case 'fetchStats':
            const { data } = await fetch(action.username)
            
            break
        default:
            return state
    }
}
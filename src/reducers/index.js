import { combineReducers } from 'redux'

import FetchStats from './FetchStats'

export default combineReducers({
    FetchStats
})
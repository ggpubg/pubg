import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import createHistory from 'history/createBrowserHistory'
import { BrowserRouter as Router } from 'react-router-dom'
import { createStore, applyMiddleware, combineReducers } from 'redux'
import { routerReducer, routerMiddleware } from 'react-router-redux'
//import 'antd/dist/antd.css'

import Reducers from './reducers'
import App from './App'
//import registerServiceWorker from './registerServiceWorker'

const history = createHistory()
const middleware = routerMiddleware(history)

const store = createStore(
    combineReducers({
        Reducers,
        router: routerReducer
    }),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    applyMiddleware(middleware)
)

ReactDOM.render(
    <Provider store={store}>
        <Router>
            <App history={history}/>
        </Router>
    </Provider>,
    document.getElementById('root')
)
//registerServiceWorker()
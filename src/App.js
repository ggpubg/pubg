import React, { Component } from 'react'
import { Switch } from 'react-router-dom'
import { withRouter } from 'react-router'
import { Layout } from 'antd'

import FancyRoute from './components/fancyRoute'
import Routes from './Routes'
import Sider from './layouts/Sider'
import Header from './layouts/Header'
import Footer from './layouts/Footer'

import './app.css'

const { Content } = Layout

class App extends Component {
    state = {
        menuMode: 'inline'
    }

    componentDidMount() {
        require('enquire.js')
        .register('only screen and (min-width: 0) and (max-width: 767px)', {
            match: () => {
                this.setState({ menuMode: 'horizontal'})
            },
            unmatch: () => {
                this.setState({ menuMode: 'inline'})
            },
        })
        window.onload = () => {
            window.body.setAttribute( 'class', 'background' )
        }
        const nprogressHiddenStyle = document.getElementById('nprogress-style')
        if (nprogressHiddenStyle) {
            this.timer = setTimeout(() => {
                nprogressHiddenStyle.parentNode.removeChild(nprogressHiddenStyle)
            }, 0)
        }
    }

    render() {
        return (
                <Layout className="App" style={{ height:'100%' }}>
                    <Header menuMode={this.state.menuMode}/>
                    <Layout>
                        <Sider menuMode={this.state.menuMode}/>
                        <Content style={{ padding: '50px 50px'}}>
                            <Switch>
                            {Routes.map((route, i) =>
                                <FancyRoute key={i} {...route} />
                            )}
                            </Switch>
                            <Footer />
                        </Content>  
                    </Layout>
                </Layout>
        )
    }
}

export default withRouter(App)
